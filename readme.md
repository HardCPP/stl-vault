

---

# Projet STL VAULT

Ce projet est une application Flask permettant de regrouper, catégoriser, consulter, trier et visionner des fichiers 3D en stéréolithographie. Le programme map l'ensemble de des projets et fichiers 3D présent sur un stockage puis se met à jour automatiquement lorsque de nouveaux élements sont créés, modifiés ou supprimés. Il permet un triage en catégorie et sous catégories personnalisable afin de favoriser les recherches pour consulter l'ensemble de vos projets 3D en quelques secondes.

## Installation

Pour installer et exécuter ce projet localement, suivez ces étapes :

1. Clonez le dépôt sur votre machine locale :

```bash
git clone https://gitlab.com/HardCPP/stl-vault.git
```

2. Accédez au répertoire du projet :

```bash
cd stl-vault/
```

3. Créez un nouvel environnement virtuel (optionnel mais recommandé) :

```bash
python -m venv venv
```

4. Activez l'environnement virtuel :

   - Sur Windows :

     ```bash
     venv\Scripts\activate
     ```

   - Sur macOS et Linux :

     ```bash
     source venv/bin/activate
     ```

5. Installez les dépendances du projet :

```bash
pip install -r requirements.txt
```

6. Configurez les paramètres du projet en éditant, si besoin, le fichier `config.json`.

## Utilisation

Une fois le projet installé et configuré, vous pouvez lancer l'application en exécutant la commande suivante :

```bash
python app.py
```

Cela démarrera l'application Flask sur votre machine locale. Vous pouvez ensuite accéder à l'application dans votre navigateur en visitant l'URL [http://localhost:5000/](http://localhost:5000/).

## Architecture du Projet

L'architecture du projet est la suivante :

```
STLVault/
│
├── app.py
├── core/
│   ├── config.py
│   ├── databaseMgr.py
├── templates/
│   └── index.html
│
└── config.json
```

## Auteur

[Auteur du Projet](https://gitlab.com/HardCPP)

## Licence

Ce projet est sous licence [MIT Licence](LICENSE).

---