import json
import os

class Config:
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(Config, cls).__new__(cls)
            cls._instance.load_config()
        return cls._instance

    def load_config(self):
        try:
            with open('config.json') as f:
                config_data = json.load(f)

            self.database_name = config_data['database']['name']
            self.database_user = config_data['database']['user']
            self.database_password = config_data['database']['password']
            self.version = config_data['version']
        except FileNotFoundError:
            raise FileNotFoundError("Configuration file 'config.json' not found.")
        except json.JSONDecodeError:
            raise ValueError("Error: Malformed configuration file 'config.json'.")
