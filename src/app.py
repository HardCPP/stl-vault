from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy

from core.config import Config

import os

app = Flask(__name__)
config = Config()

app.config['SQLALCHEMY_DATABASE_URI'] = f"sqlite:///{config.database_name}"

db = SQLAlchemy(app)

@app.route('/')
def index():
    return render_template('index.html', version=config.version)

if __name__ == '__main__':
    app.run(debug=True)

